﻿#include <iostream>
#include <time.h>

using namespace std;

int main()
{
    // получаем текущую дату
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int day = buf.tm_mday;

    const int N = 5;
    int arr[N][N];

    // заполняем массив
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            arr[i][j] = i + j;
        }
    }

    // выводим массив в консоль
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }

    // ищем сумму элементов в строке с нужным индексом
    int row_index = day % N;
    int sum = 0;
    for (int j = 0; j < N; j++)
    {
        sum += arr[row_index][j];
    }

    // выводим результат
    cout << "Sum of elements in row " << row_index << ": " << sum << endl;

    return 0;
}